import discord
import asyncio
import _thread

client = discord.Client()
music_lineup = None

class MusicQue():
    def __init__(self, channel, v_channel):
        self.current_queue = asyncio.Queue()
        self.buffer_queue = asyncio.Queue()
        self.channel = channel
        self.v_channel = v_channel
        _thread.start_new_thread(self.run, ())

    def worker(self):
        while True:
            self.execute()

    def add(self, item):
        n_item = item
        # self.buffer_queue.put(n_item)
        self.buffer_queue.put("test")
        print("Adding 2 Q size is", self.buffer_queue.qsize(), self.buffer_queue.empty(), item)

    def flush_buffer(self):
        print("Flushing Buffer")
        self.current_queue = self.buffer_queue
        while not self.buffer_queue.empty():
            self.buffer_queue.get()
            self.buffer_queue.task_done()

    async def execute(self):
        self.flush_buffer()
        voice_client = await client.join_voice_channel(self.v_channel)
        while not self.current_queue.empty():
            data_tuple = tuple(self.current_queue.get())
            # 0 = text, 1 = file, 2 = url
            if data_tuple[1] == 0:
                self.send_text(self.channel, data_tuple[0])
            elif data_tuple[1] == 1:
                self.send_text(voice_client, data_tuple[0])
            elif data_tuple[1] == 2:
                self.send_text(voice_client, data_tuple[0])

            self.current_queue.task_done()
        await voice_client.disconnect()

    async def send_text(self, channel, message):
        print("Sending message")
        await client.send_message(channel, message)

    async def send_voice_file(self, vclient, file):
        print("Sending Voice File")
        player = await vclient.create_ffmpeg_player(file)
        player.start()

    async def send_voice_url(self, vclient, url):
        print("Sending Voice URL")
        player = await vclient.create_ytdl_player(url)
        player.start()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    await client.change_status(game=discord.Game(name="Dankistan"))

@client.event
async def on_message(message):
    global music_lineup

    if message.content.startswith('~dank'):
        print("Configuring......", message.channel.name, message.author.voice_channel.name)
        music_lineup = MusicQue(message.channel, message.author.voice_channel)
    # elif music_lineup is None:
    #     await client.send_message(message.channel, 'Please configure channels using \'~dank\'')

    elif message.content.startswith('pg'):
        print("adding pg")
        music_lineup.add( ('https://www.youtube.com/watch?v=JeimE8Wz6e4', 2) )

    elif message.content.startswith('cena'):
        print("adding cena")
        music_lineup.add( ('https://www.youtube.com/watch?v=Uufq_PFXbpA', 2) )

    elif message.content.startswith('gay'):
        print("adding gay")
        music_lineup.add( ('https://www.youtube.com/watch?v=Fz50hqWrHUY', 2) )

    elif message.content.startswith('tibs'):
        print("playing tibbers")
        music_lineup.add( ('tibs.aac', 1) )

# @client.event
# async def on_message(message):
#     if message.content.startswith('o shit waddup'):
#         print("printing owhat")
#         await client.send_message(message.channel, 'HERE COMES DAT BOI', tts=True)

#     elif message.content.startswith('pg'):
#         print("adding pg")
#         await play_video(message.author.voice_channel, 'https://www.youtube.com/watch?v=JeimE8Wz6e4')

#     elif message.content.startswith('cena'):
#         print("adding cena")
#         await play_video(message.author.voice_channel, 'https://www.youtube.com/watch?v=Uufq_PFXbpA')

#     elif message.content.startswith('gay'):
#         print("adding gay")
#         await play_video(message.author.voice_channel, 'https://www.youtube.com/watch?v=Fz50hqWrHUY')

#     elif message.content.startswith('tibs'):
#         print("playing tibbers")
#         await play_file(message.author.voice_channel, "tibs.aac")

# async def play_video(channel, link):
#     voice_client = await client.join_voice_channel(channel)
#     player = await voice_client.create_ytdl_player(link, after=lambda: clean_up(voice_client))
#     player.start()

# async def play_file(channel, link):
#     voice_client = await client.join_voice_channel(channel)
#     player = voice_client.create_ffmpeg_player(link, after=lambda: clean_up(voice_client))
#     player.start()

# def clean_up(voice_client):
#     coro = voice_client.disconnect()
#     fut = asyncio.run_coroutine_threadsafe(coro, client.loop)
#     print("Disconnecting from Server")
#     try:
#         fut.result()
#     except:
#         pass

discord.opus.load_opus("libopus.0.dylib")
client.run('MTkyNTA5MzU0OTc5NzUzOTg0.CkJ3zg.Q2p6xIuPYNuyLcz8hUyTdwtTkQc')